/*===========================================================================
  Copyright (C) 2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.applications.rainbow;

import org.slf4j.event.Level;

import net.sf.okapi.applications.IOkapiLoggerAppender;
import net.sf.okapi.applications.rainbow.lib.ILog;

public class RainbowLoggerAppender implements IOkapiLoggerAppender {
	private static Level level = Level.INFO;
	private ILog log = null;

	@Override
	public void log(String loggerName, Level level, String msg, Throwable throwable) {
		if (log == null)
			return;

		Level lev = level;
		switch (level) {
			case ERROR:
				log.error(msg);
				if ( throwable != null ) {
					log.message(" @ " + throwable.toString()); //$NON-NLS-1$
				}
				break;
			case WARN:
				log.warning(msg);
				break;
			default: // INFO and below
				log.message(msg);
		}
	}

	@Override
	public Level getLevel() {
		return level;
	}

	@Override
	public void setLevel(Level newLevel) {
		level = newLevel;
	}

	public void setLogger(ILog log) {
		this.log = log;
	}
}
