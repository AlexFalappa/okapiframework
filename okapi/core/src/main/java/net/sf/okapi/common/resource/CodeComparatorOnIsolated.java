/*===========================================================================
  Copyright (C) 2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common.resource;

import java.util.Comparator;

public class CodeComparatorOnIsolated implements Comparator<Code> {
	public static final CodeComparatorOnTagType CMP_TAG_TYPE = new CodeComparatorOnTagType();

	private final CodeMatches codematches;
	private final boolean matchIsolatedOnly;
	private int index;
	private int fromIndex;

	public CodeComparatorOnIsolated(CodeMatches codeMatches) {
		this(codeMatches, true);
	}

	public CodeComparatorOnIsolated(CodeMatches codeMatches, boolean matchIsolatedOnly) {
		this.codematches = codeMatches;
		this.matchIsolatedOnly = matchIsolatedOnly;
	}

	public void setToIndex(int index) {
		this.index = index;
	}

	public void setFromIndex(int index) {
		this.fromIndex = index;
	}

	@Override
	public int compare(Code c1, Code c2) {
		if (c1 == c2) {
			return 0;
		}

		if (c1 == null || c2 == null) {
			return -1;
		}

		boolean isC1Isolated = codematches.isToIsolated(index);
		boolean isC2Isolated = codematches.isFromIsolated(fromIndex);

		if (matchIsolatedOnly) {
			// match on isolated type only
			if ((isC1Isolated && isC2Isolated) || (!isC1Isolated && !isC2Isolated)) {
				return 0;
			}
		} else {
			// match on TagType and isolated matches PLACEHOLDER
			boolean tagTypeMatch = CMP_TAG_TYPE.compare(c1, c2) == 0;
			if (tagTypeMatch) {
				return 0;
			} else if (c1.getTagType() == TextFragment.TagType.PLACEHOLDER && isC2Isolated) {
				return 0;
			} else if (c2.getTagType() == TextFragment.TagType.PLACEHOLDER && isC1Isolated) {
				return 0;
			}
		}

		return -1;
	}
}
