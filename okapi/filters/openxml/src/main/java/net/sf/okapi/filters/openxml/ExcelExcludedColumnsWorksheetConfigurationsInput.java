/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @see ConditionalParameters#tsExcelExcludedColumns for more information
 */
@Deprecated
final class ExcelExcludedColumnsWorksheetConfigurationsInput implements WorksheetConfigurations.Input {
    private static final String DEFAULT_STRING_VALUE = "";

    private final WorkbookFragments workbookFragments;
    private final Set<String> excelExcludedColumns;
    private List<WorksheetConfiguration> worksheetConfigurations;

    ExcelExcludedColumnsWorksheetConfigurationsInput(final WorkbookFragments workbookFragments, final Set<String> excelExcludedColumns) {
        this.workbookFragments = workbookFragments;
        this.excelExcludedColumns = excelExcludedColumns;
    }

    @Override
    public Iterator<WorksheetConfiguration> read() {
        if (null != this.worksheetConfigurations) {
            return Collections.emptyIterator();
        }
        this.worksheetConfigurations = new LinkedList<>();
        for (final String excludedColumn : this.excelExcludedColumns) {
            if (DEFAULT_STRING_VALUE.equals(excludedColumn)) {
                continue;
            }
            this.worksheetConfigurations.add(new ExcelExcludedColumnWorksheetConfiguration(this.workbookFragments, excludedColumn));
        }
        return this.worksheetConfigurations.iterator();
    }
}
