/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

interface WorkbookFragments {
	List<String> worksheetNames();
	String localisedWorksheetNameFor(final String worksheetName);
	String localisedWorksheetNameFor(final int worksheetNumber);
	boolean worksheetHiddenFor(final String worksheetName);
	void readWith(final XMLEventReader eventReader) throws XMLStreamException;

	final class Default implements WorkbookFragments {
		private static final String EMPTY = "";
		private static final String WORKSHEET_NAME_HAS_NOT_BEEN_FOUND = "The provided worksheet name has not been found: ";
		private static final String WORKBOOK = "workbook";
		private static final String SHEET = "sheet";
		private static final QName SHEET_NAME = new QName("name");
		private static final QName SHEET_STATE = new QName("state");
		private static final String ID = "id";
		private static final String HIDDEN = "hidden";

		private final ConditionalParameters conditionalParameters;
		private final Relationships relationships;
		private final List<String> worksheetNames;
		private final Map<String, String> localisedWorksheetNames;
		private final Set<String > hiddenWorksheets;

		private QName sheet;
		private QName id;

		Default(
			final ConditionalParameters conditionalParameters,
			final Relationships relationships
		) {
			this(
				conditionalParameters,
				relationships,
				new ArrayList<>(),
				new HashMap<>(),
				new HashSet<>()
			);
		}

		Default(
			final ConditionalParameters conditionalParameters,
			final Relationships relationships,
			final List<String> worksheetNames,
			final Map<String, String> localisedWorksheetNames,
			final Set<String> hiddenWorksheets
		) {
			this.conditionalParameters = conditionalParameters;
			this.relationships = relationships;
			this.worksheetNames = worksheetNames;
			this.localisedWorksheetNames = localisedWorksheetNames;
			this.hiddenWorksheets = hiddenWorksheets;
		}

		@Override
		public List<String> worksheetNames() {
			return this.worksheetNames;
		}

		@Override
		public String localisedWorksheetNameFor(final String worksheetName) {
			if (!this.localisedWorksheetNames.containsKey(worksheetName)) {
				throw new IllegalStateException(Default.WORKSHEET_NAME_HAS_NOT_BEEN_FOUND.concat(worksheetName));
			}
			return this.localisedWorksheetNames.get(worksheetName);
		}

		@Override
		public String localisedWorksheetNameFor(final int worksheetNumber) {
			if (this.worksheetNames.size() < worksheetNumber) {
				return EMPTY;
			}
			final String name = this.worksheetNames.get(worksheetNumber - 1);
			return null == name
				? EMPTY
				: this.localisedWorksheetNames.get(name);
		}

		@Override
		public boolean worksheetHiddenFor(final String worksheetName) {
			if (!this.worksheetNames.contains(worksheetName)) {
				return false;
			}
			return this.hiddenWorksheets.contains(worksheetName);
		}

		@Override
		public void readWith(final XMLEventReader reader) throws XMLStreamException {
			while (reader.hasNext()) {
				final XMLEvent e = reader.nextEvent();
				if (!e.isStartElement()) {
					continue;
				}
				final StartElement el = e.asStartElement();
				if (Default.WORKBOOK.equals(el.getName().getLocalPart())) {
					qualifyNames(el);
				} else if (el.getName().equals(this.sheet)) {
					if (null == this.id) {
						qualifyIdName(el);
					}
					final String worksheetName = relationshipTargetFor(
						el.getAttributeByName(this.id).getValue()
					);
					this.worksheetNames.add(worksheetName);
					this.localisedWorksheetNames.put(
						worksheetName,
						el.getAttributeByName(Default.SHEET_NAME).getValue()
					);
					final String state = XMLEventHelpers.getAttributeValue(el, Default.SHEET_STATE);
					if (!this.conditionalParameters.getTranslateExcelHidden()
						&& Default.HIDDEN.equals(state)) {
						this.hiddenWorksheets.add(worksheetName);
					}
				}
			}
		}

		private void qualifyNames(final StartElement startElement) {
			this.sheet = new QName(
				startElement.getName().getNamespaceURI(),
				SHEET,
				startElement.getName().getPrefix()
			);
			qualifyIdName(startElement);
		}

		private void qualifyIdName(final StartElement startElement) {
			final String namespaceUri = startElement.getNamespaceURI(Namespace.PREFIX_R);
			if (null == namespaceUri) {
				return;
			}
			this.id = new QName(
				namespaceUri, ID, Namespace.PREFIX_R
			);
		}

		private String relationshipTargetFor(final String id) {
			final Relationships.Rel rel = this.relationships.getRelById(id);
			if (rel == null) {
				throw new IllegalStateException(
					"A non-existent relationship is requested: " + id
				);
			}
			return rel.target;
		}
	}
}
