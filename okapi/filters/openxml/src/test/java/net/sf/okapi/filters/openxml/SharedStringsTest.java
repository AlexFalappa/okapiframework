package net.sf.okapi.filters.openxml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class SharedStringsTest {

    @Test
    public void reordered() {
        final SharedStrings ssm = new SharedStrings();
        final String worksheetName = "w";
        final boolean excluded = true;
        final MetadataContext metadataContext = MetadataContext.NONE;
        ssm.add(
            new SharedStrings.Item(
                4,
                ssm.nextIndex(),
                Collections.emptyList(),
                worksheetName,
                new CellReferencesRange(new CellReference("A1")),
                excluded,
                metadataContext
            )
        );
        ssm.add(
            new SharedStrings.Item(
                1,
                ssm.nextIndex(),
                Collections.emptyList(),
                worksheetName,
                new CellReferencesRange(new CellReference("B1")),
                excluded,
                metadataContext
            )
        );
        ssm.add(
            new SharedStrings.Item(
                3,
                ssm.nextIndex(),
                Collections.emptyList(),
                worksheetName,
                new CellReferencesRange(new CellReference("C1")),
                excluded,
                metadataContext
            )
        );
        ssm.add(
            new SharedStrings.Item(
                2,
                ssm.nextIndex(),
                Collections.emptyList(),
                worksheetName,
                new CellReferencesRange(new CellReference("D1")),
                excluded,
                metadataContext
            )
        );
        final List<SharedStrings.Item> expected = new ArrayList<>();
        expected.add(
            new SharedStrings.Item(
                4,
                0,
                Collections.emptyList(),
                worksheetName,
                new CellReferencesRange(new CellReference("A1")),
                excluded,
                metadataContext
            )
        );
        expected.add(
            new SharedStrings.Item(
                1,
                1,
                Collections.emptyList(),
                worksheetName,
                new CellReferencesRange(new CellReference("B1")),
                excluded,
                metadataContext
            )
        );
        expected.add(
            new SharedStrings.Item(
                3,
                2,
                Collections.emptyList(),
                worksheetName,
                new CellReferencesRange(new CellReference("C1")),
                excluded,
                metadataContext
            )
        );
        expected.add(
            new SharedStrings.Item(
                2,
                3,
                Collections.emptyList(),
                worksheetName,
                new CellReferencesRange(new CellReference("D1")),
                excluded,
                metadataContext
            )
        );
        assertEquals(expected, ssm.items());
    }
}
