package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.FileLocation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.DifferenceEvaluators;
import org.xmlunit.matchers.CompareMatcher;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class WorksheetTest {
	private static final String WORKSHEET_NAME = "test";
	private final XMLFactories xmlfactories = new XMLFactoriesForTest();
	private FileLocation root;

	@Before
	public void setUp() {
		root = FileLocation.fromClass(getClass());
	}

	@Test
	public void test() throws Exception {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		final Worksheet worksheet = new Worksheet.Default(
			conditionalParameters,
			xmlfactories.getEventFactory(),
			new SharedStrings(),
			new ExcelStyles(),
			WORKSHEET_NAME,
			new WorksheetFragments.Default(
				conditionalParameters.getTranslateExcelHidden(),
				true
			)
		);
		read(worksheet, "/xlsx_parts/sheet1.xml");
		StringWriter sw = new StringWriter();
		XMLEventWriter writer = xmlfactories.getOutputFactory().createXMLEventWriter(sw);
		worksheet.writeWith(writer);
		writer.close();
		assertThat(Input.fromStream(root.in("/xlsx_parts/gold/Rewritten_sheet1.xml").asInputStream()),
				CompareMatcher.isIdenticalTo(sw.toString())
						.withDifferenceEvaluator(DifferenceEvaluators.ignorePrologDifferences()));
	}

	@Test
	public void testExcludeColors() throws Exception {
		SharedStrings sharedStrings = new SharedStrings();
		ExcelStyles styles = new ExcelStyles();
		styles.parse(xmlfactories.getInputFactory().createXMLEventReader(
				root.in("/xlsx_parts/rgb_styles.xml").asInputStream(), "UTF-8"));
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		conditionalParameters.tsExcelExcludedColors.add("FF800000");
		conditionalParameters.tsExcelExcludedColors.add("FFFF0000");
		final Worksheet worksheet = new Worksheet.Default(
			conditionalParameters,
			xmlfactories.getEventFactory(),
			sharedStrings,
			styles,
			WORKSHEET_NAME,
			new WorksheetFragments.Default(
				conditionalParameters.getTranslateExcelHidden(),
				false
			)
		);
		read(worksheet, "/xlsx_parts/rgb_sheet1.xml");
		List<SharedStrings.Item> entries = sharedStrings.items();
		assertTrue(entries.get(0).excluded());  // excluded due to FF800000
		assertTrue(entries.get(1).excluded());  // excluded due to FFFF0000
		assertFalse(entries.get(2).excluded());
		assertFalse(entries.get(3).excluded());
		assertFalse(entries.get(4).excluded());
		assertFalse(entries.get(5).excluded());
		assertFalse(entries.get(6).excluded());
		assertFalse(entries.get(7).excluded());
		assertFalse(entries.get(8).excluded());
		assertFalse(entries.get(9).excluded());
	}

	@Test
	public void testExcludeHiddenCells() throws Exception {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		final SharedStrings sharedStrings = new SharedStrings();
		final Worksheet worksheet = new Worksheet.Default(
			conditionalParameters,
			xmlfactories.getEventFactory(),
			sharedStrings,
			new ExcelStyles(),
			WORKSHEET_NAME,
			new WorksheetFragments.Default(
				conditionalParameters.getTranslateExcelHidden(),
				false
			)
		);
		read(worksheet, "/xlsx_parts/worksheet-hiddenCells.xml");
		List<SharedStrings.Item> entries = sharedStrings.items();
		assertFalse(entries.get(0).excluded());
		assertTrue(entries.get(1).excluded());
		assertTrue(entries.get(2).excluded());
		assertTrue(entries.get(3).excluded());
	}

	@Test
	public void testExposeHiddenCells() throws Exception {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		final SharedStrings sharedStrings = new SharedStrings();
		final Worksheet worksheet = new Worksheet.Default(
			conditionalParameters,
			xmlfactories.getEventFactory(),
			sharedStrings,
			new ExcelStyles(),
			WORKSHEET_NAME,
			new WorksheetFragments.Default(
				conditionalParameters.getTranslateExcelHidden(),
				false
			)
		);
		read(worksheet, "/xlsx_parts/worksheet-hiddenCells.xml");
		List<SharedStrings.Item> entries = sharedStrings.items();
		assertFalse(entries.get(0).excluded());
		assertTrue(entries.get(1).excluded());
		assertTrue(entries.get(2).excluded());
		assertTrue(entries.get(3).excluded());
	}

	private void read(Worksheet worksheet, String resourceName) throws Exception {
		XMLEventReader reader = xmlfactories.getInputFactory().createXMLEventReader(
				root.in(resourceName).asInputStream(), StandardCharsets.UTF_8.name());
		worksheet.readWith(reader);
		reader.close();
	}
}
