/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.xliff2;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.annotation.GenericAnnotation;
import net.sf.okapi.common.annotation.GenericAnnotationType;
import net.sf.okapi.common.annotation.GenericAnnotations;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.StartGroup;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextPart;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.filters.xliff2.util.PropertiesMapper;
import net.sf.okapi.lib.xliff2.core.CTag;
import net.sf.okapi.lib.xliff2.core.Fragment;
import net.sf.okapi.lib.xliff2.core.MTag;
import net.sf.okapi.lib.xliff2.core.MidFileData;
import net.sf.okapi.lib.xliff2.core.Part;
import net.sf.okapi.lib.xliff2.core.StartGroupData;
import net.sf.okapi.lib.xliff2.core.Tag;
import net.sf.okapi.lib.xliff2.core.TagType;
import net.sf.okapi.lib.xliff2.core.Unit;
import net.sf.okapi.lib.xliff2.metadata.IMetadataItem;
import net.sf.okapi.lib.xliff2.metadata.Meta;
import net.sf.okapi.lib.xliff2.metadata.MetaGroup;
import net.sf.okapi.lib.xliff2.metadata.Metadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Stack;

public class X2ToOkpConverter {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final LocaleId trgLoc;

	/**
	 * Creates a new converter object.
	 * 
	 * @param trgLoc the target locale.
	 */
	public X2ToOkpConverter(LocaleId trgLoc) {
		this.trgLoc = trgLoc;
	}

	public DocumentPart convert(MidFileData midFileData) {
		DocumentPart documentPart = new DocumentPart();

		// Transfer XLIFF 2 metadata module elements into context group annotations
		if (midFileData.hasMetadata()) {
			GenericAnnotation contextGroup = metadataToContextGroup(midFileData.getMetadata());
			documentPart.setAnnotation(new GenericAnnotations(contextGroup));
		}

		return documentPart;
	}

	public StartGroup convert(StartGroupData sgd, String parentId) {
		StartGroup sg = new StartGroup(parentId, sgd.getId());

		sg.setId(sgd.getId());
		sg.setIsTranslatable(sgd.getTranslate());
		sg.setName(sgd.getName());
		sg.setType(sgd.getType());

		// Transfer XLIFF 2 metadata module elements into context group annotations
		if (sgd.hasMetadata()) {
			GenericAnnotation contextGroup = metadataToContextGroup(sgd.getMetadata());
			sg.setAnnotation(new GenericAnnotations(contextGroup));
		}

		return sg;
	}

	// Converts XLIFF 2 metadata module elements into TextUnit annotations
	private GenericAnnotation metadataToContextGroup(Metadata md) {
		GenericAnnotation contextGroup = new GenericAnnotation(GenericAnnotationType.MISC_METADATA);
		int i = 0;
		for (MetaGroup metaGroup : md) {
			addMeta(metaGroup, contextGroup, String.valueOf(i));
			i++;
		}

		return contextGroup;
	}

	private void addMeta(IMetadataItem fromMetadata, GenericAnnotation toContextGroup, String sequenceId) {
		if (fromMetadata.isGroup()) {
			MetaGroup metaGroup = (MetaGroup) fromMetadata;
			Iterator<IMetadataItem> metaIterator = metaGroup.iterator();
			int i = 0;
			while (metaIterator.hasNext()) {
				IMetadataItem next = metaIterator.next();
				addMeta(next, toContextGroup, sequenceId + "." + i);
				i++;
			}
		} else {
			Meta meta = (Meta) fromMetadata;
			// check for duplicate types
			if (toContextGroup.getString(meta.getType()) != null) {
				// use the group and meta sequence numbers to make unique
				toContextGroup.setString(meta.getType() + "." + sequenceId, meta.getData());
			} else {
				toContextGroup.setString(meta.getType(), meta.getData());
			}
		}
	}

	public ITextUnit convert(Unit unit) {
		ITextUnit tu = new TextUnit(unit.getId());
		tu.setName(unit.getName());
		tu.setType(unit.getType());
		tu.setPreserveWhitespaces(unit.getPreserveWS());
		tu.setIsTranslatable(unit.getTranslate());

		// Transfer XLIFF 2 metadata module elements into context group annotations
		if (unit.hasMetadata()) {
			GenericAnnotation contextGroup = metadataToContextGroup(unit.getMetadata());
			tu.setAnnotation(new GenericAnnotations(contextGroup));
		}

		TextContainer src = tu.getSource();
		TextContainer trg = null;

		// Do we have at least one target part?
		boolean hasTarget = false;
		for (Part part : unit) {
			if (part.hasTarget()) {
				hasTarget = true;
				break;
			}
		}
		// Transfer the target if needed
		if (hasTarget) {
			trg = tu.createTarget(trgLoc, false, IResource.CREATE_EMPTY);
		}

		// Must process the src and trg containers together so segments have same id
		// needed for matching later
		convert(unit, src, trg);
		return tu;
	}

	private void convert(Unit unit, TextContainer src, TextContainer trg) {
		List<TextPart> srcTextParts = new ArrayList<>();
		List<TextPart> trgTextParts = new ArrayList<>();
		Set<String> partIds = new HashSet<>();

		for (Part part : unit) {
			// use original xliff2 id if available
			TextPart stp;
			TextPart ttp;
			String id = generateId(part, partIds);
			partIds.add(id);
			if (part.isSegment()) {
				stp = convertToSegment(part, id);
				ttp = convertToSegment(part, id);
			} else {
				// ignorable or inter-segment text
				stp = convertToTextPart(part, id);
				ttp = convertToTextPart(part, id);
			}

			stp.setPreserveWhitespaces(part.getPreserveWS());
			ttp.setPreserveWhitespaces(part.getPreserveWS());

			convert(part.getSource(), stp);
			convert(part.getTarget(), ttp);

			srcTextParts.add(stp);
			trgTextParts.add(ttp);

			PropertiesMapper.setPartProperties(part, stp);
			PropertiesMapper.setPartProperties(part, ttp);
		}

		src.setParts(srcTextParts.toArray(new TextPart[0]));
		if (trg != null) {
			trg.setParts(trgTextParts.toArray(new TextPart[0]));
		}
	}

	/*
	id's cannot be random - make deterministic based on source text
	and avoid duplicates with id hash
	 */
	private String generateId(Part part, Set<String> partIds) {
		if (Util.isEmpty(part.getId())) {
			int hc = Math.abs(part.getSource().toString().hashCode());
			String id = String.valueOf(hc);
			// check for duplicates
			while (partIds.contains(id)) {
				id = String.valueOf(++hc);
			}
			return id;
		}

		return part.getId();
	}

	private Segment convertToSegment(Part part, String id) {
		Segment s = new Segment(id);
		s.setOriginalId(part.getId());
		return s;
	}

	private TextPart convertToTextPart(Part part, String id) {
		TextPart tp = new TextPart(id, null);
		tp.setOriginalId(part.getId());
		return tp;
	}

	private void convert(Fragment frag, TextPart part) {
		if (frag == null) {
			return;
		}

		final Stack<String> mrkStack = new Stack<>();
		TextFragment tf = part.text;
		for (Object obj : frag) {
			if (obj instanceof String) {
				tf.append((String) obj);
			} else if (obj instanceof CTag) {
				CTag ctag = (CTag) obj;
				Code code = new Code(ctag.getType());

				final String id = ctag.getId();
				int okapiId = Math.abs(id.hashCode());
				code.setId(okapiId);
				code.setDeleteable(ctag.getCanDelete());
				code.setCloneable(ctag.getCanCopy());
				code.setData(ctag.getData());
				code.setDisplayText(ctag.getDisp());
				code.setOriginalId(ctag.getId());
				code.setTagType(convertTagType(ctag.getTagType()));
				if (ctag.getType() != null) {
					code.setType(ctag.getType());
				}

				PropertiesMapper.setCodeProperties(ctag, code);

				tf.append(code);
			} else if (obj instanceof MTag) {
				MTag mtag = (MTag) obj;
				Code c = new Code();
				c.setType(mtag.getType());
				if (mtag.getTagType() == TagType.OPENING) {
					c.setOriginalId(mtag.getId());
					c.setTagType(TextFragment.TagType.OPENING);
					mrkStack.push(mtag.getId());
				} else {
					c.setOriginalId(mrkStack.pop());
					c.setTagType(TextFragment.TagType.CLOSING);
				}
				PropertiesMapper.setCodeProperties(mtag, c);
				tf.append(c);
			}
		}
	}

	private static TextFragment.TagType convertTagType(net.sf.okapi.lib.xliff2.core.TagType tagType) {
		switch (tagType) {
			case CLOSING:
				return TextFragment.TagType.CLOSING;
			case OPENING:
				return TextFragment.TagType.OPENING;
			case STANDALONE:
				return TextFragment.TagType.PLACEHOLDER;
			default:
				LoggerFactory.getLogger(PropertiesMapper.class).warn("TagType {} unrecognized. Treating it as {}.", tagType,
						net.sf.okapi.lib.xliff2.core.TagType.STANDALONE);
				return TextFragment.TagType.PLACEHOLDER;

		}
	}
}
